> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Project 2 Requirements:

*Sub-Heading:*

1. Implement CRUD functionality
2. Provide screenshot of successful user form entry
3. Provide screenshot of successful data modification
4. Provide screenshot of database changes

#### Assignment Screenshot and Links:

*Valid User Form Entry*:

![Valid User Form Entry](img/p2.PNG "Valid User Form Entry")

*Passed Validation*:

![Passed Validation](img/p2_thanks.PNG "Passed Validation")

*Display Data*:

![Display Data](img/p2_display.PNG "Display Data")

*Modify Form*:

![Modify Form](img/p2_modify.PNG "Modify Form")

*Modified Data*:

![Modified Data](img/p2_modified.PNG "Modified Data")

*Delete Warning*:

![Delete Warning](img/p2_delete.PNG "Delete Warning")

*Associated Database Changes*:

![Associated Database Changes](img/p2_mysql.PNG "Associated Database Changes")