> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Application Development

## Devin Hartley

### Class Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL Workbench
    - Create ebookshop Database
    - Create Java Servlets
    - Compile Java Servlets
    - Provide Screenshots of Working Servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based on provided business rules
    - Forward engineer ERD
    - Provide screenshot of ERD
    - Provide screenshot of a3 index

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Implement functional client-side data validation
    - Provide screenshot of main LIS4368 portal
    - Provide screenshot of failed validation
    - Provide screenshot of successful validation

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Implement basic server-side data validation
    - Provide screenshot of failed validation
    - Provide screenshot of successful validation
    - Provide screenshots of assignment skill sets

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Provide screenshot of data entry
    - Provide screenshot of successful validation
    - Provide screenshot of MySQL table data
    - Provide screenshots of assignment skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Implement CRUD functionality
    - Provide screenshot of successful user form entry
    - Provide screenshot of successful data modification
    - Provide screenshot of database changes