> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Assignment 3 Requirements:

*Sub-Heading:*

1. Create ERD based on provided business rules
2. Forward engineer ERD
3. Provide screenshot of ERD
4. Provide screenshot of a3 index

#### Assignment Screenshot and Links:

*Screenshot A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

*Screenshot of A3 Website*:

![A2 Website Screenshot](img/a3web.PNG)