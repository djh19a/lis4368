-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema djh19a
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `djh19a` ;

-- -----------------------------------------------------
-- Schema djh19a
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `djh19a` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `djh19a` ;

-- -----------------------------------------------------
-- Table `djh19a`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `djh19a`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `djh19a`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `djh19a`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `djh19a`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `djh19a`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `djh19a`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `djh19a`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `djh19a`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `djh19a`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `djh19a`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `djh19a`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `djh19a`;
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Bob\'s Pet Store', 'Main St.', 'Macclenny', 'FL', '32063', 9041234567, 'bob@gmail.com', 'www.bobspetstore.com', 5000.00, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Joe\'s Pet Store', 'First St.', 'Jacksonville', 'FL', '32034', 9049876543, 'joe@gmail.com', 'www.joespetstore.com', 6000.50, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Jim\'s Pet Store', 'Second St.', 'Jacksonville', 'FL', '32219', 9042345678, 'jim@gmail.com', 'www.jimspetstore.com', 5000.50, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'John\'s Pet Store', 'Third St.', 'Macclenny', 'FL', '32063', 9048765432, 'john@gmail.com', 'www.johnspetstore.com', 5500.00, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'Robert\'s Pet Store', 'Fourth St.', 'Tampa', 'FL', '33601', 8133456789, 'robert@gmail.com', 'www.robertspetstore.com', 5500.50, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'Ronald\'s Pet Store', 'Fifth St.', 'Tallahassee', 'FL', '32310', 8501111111, 'ronald@gmail.com', 'www.ronaldspetstore.com', 5600.00, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Don\'s Pet Store', 'Sixth St.', 'Macclenny', 'FL', '32063', 9042222222, 'don@gmail.com', 'www.donspetstore.com', 6000.00, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Trevor\'s Pet Store', 'TIAA Bank Field Dr.', 'Jacksonville', 'FL', '32202', 9043333333, 'trevor@gmail.com', 'www.trevorspetstore.com', 2000.50, 'recently opened');
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Lawrence\'s Pet Store', 'TIAA Bank Field Dr.', 'Jacksonville', 'FL', '32202', 9044444444, 'lawrence@gmail.com', 'www.lawrencespetstore.com', 4000.65, NULL);
INSERT INTO `djh19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Fred\'s Pet Store', 'St. Augustine Rd.', 'St. Augustine', 'FL', '32080', 9045555555, 'fred@gmail.com', 'www.fredspetstore.com', 7000.35, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `djh19a`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `djh19a`;
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'John', 'Johnson', 'First St.', 'St. Augustine', 'FL', '32080', 9045674321, 'jjohnson@gmail.com', 100.00, 300.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'William', 'Williams', 'Second St.', 'Jacksonville', 'FL', '32219', 9043012459, 'william2@gmail.com', 0.00, 360.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Robert', 'Roberts', 'Third St.', 'Jacksonville', 'FL', '32202', 9049049049, 'robert2@gmail.com', 0.00, 200.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'James', 'Jameson', 'Fourth St.', 'Macclenny', 'FL', '32063', 9045212380, 'jjameson@gmail.com', 0.00, 200.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Frank', 'Francis', 'Fifth St.', 'Tallahassee', 'FL', '32310', 8509472135, 'frank@gmail.com', 0.00, 125.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Ronald', 'McDonald', 'Sixth St.', 'Macclenny', 'FL', '32063', 9041478654, 'notthatronaldmcdonald@gmail.com', 0.00, 300.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Thomas', 'Thomas', 'Seventh St.', 'Jacksonville', 'FL', '32034', 9044219033, 'thomas2@gmail.com', 0.00, 300.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Andrew', 'Andrews', 'Eighth St.', 'Jacksonville', 'FL', '32219', 9047812564, 'andrew2@gmail.com', 0.00, 125.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Adam', 'Adams', 'Ninth St.', 'Macclenny', 'FL', '32063', 9044812162, 'adam2@gmail.com', 10.00, 115.00, NULL);
INSERT INTO `djh19a`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Jackson', 'Jackson', 'Main St.', 'Tampa', 'FL', '33601', 8133692468, 'jackson2@gmail.com', 50.00, 125.00, 'balance expected to be paid soon');

COMMIT;


-- -----------------------------------------------------
-- Data for table `djh19a`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `djh19a`;
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 10, 1, 'Beagle', 'm', 200.00, 300.00, 10, 'brown', '2021-02-14', 'y', 'n', 'good boy');
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 9, 2, 'Golden Retriever', 'f', 240.00, 360.00, 12, 'gold', '2021-01-12', 'y', 'n', 'good girl');
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 8, 3, 'American Shorthair', 'm', 150.00, 200.00, 20, 'black', '2021-01-07', 'y', 'y', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 7, 4, 'American Shorthair', 'f', 150.00, 200.00, 16, 'white', '2020-12-24', 'y', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 6, 5, 'Turtle', 'm', 100.00, 125.00, 30, 'green', '2020-12-17', 'n', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 5, 10, 'Snake', 'f', 100.00, 125.00, 24, 'brown', '2021-01-01', 'n', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 4, 9, 'Hamster', 'm', 75.00, 115.00, 4, 'gold', '2020-10-31', 'n', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 3, 8, 'Ferret', 'f', 100.00, 125.00, 8, 'grey', '2021-01-31', 'y', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 2, 7, 'Dachshund', 'm', 200.00, 300.00, 10, 'brown', '2021-02-01', 'y', 'n', NULL);
INSERT INTO `djh19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 1, 6, 'Chihuahua', 'f', 200.00, 300.00, 12, 'black', '2021-02-07', 'y', 'n', NULL);

COMMIT;

