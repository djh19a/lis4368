> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Assignment 5 Requirements:

*Sub-Heading:*

1. Provide screenshot of data entry
2. Provide screenshot of successful data validation
3. Provide screenshot of MySQL table data
4. Provide screenshots of assignment skill sets

#### Assignment Screenshot and Links:

*Screenshot of A5 Data Entry*:

![A5 Data Entry](img/a5.PNG "Data Entry")

*Screenshot of A5 Passed Validation*:

![A5 Passed Validation](img/a5_success.PNG "Passed Validation")

*Screenshot of A5 MySQL Data*:

![A5 MySQL Data](img/a5_table.PNG "MySQL Data")

*Screenshots of Assignment Skill Sets*:

![SS13](img/SS13.PNG "Skill Set 13")

![SS14](img/SS14.PNG "Skill Set 14")

![SS15](img/SS15.PNG "Skill Set 15")