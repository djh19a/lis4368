> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Project 1 Requirements:

*Sub-Heading:*

1. Implement functional client-side data validation
2. Provide screenshot of main LIS4368 portal
3. Provide screenshot of failed validation
4. Provide screenshot of successful validation

#### Assignment Screenshot and Links:

*LIS4368 Portal (Main/Splash Page)*:

![LIS4368 Portal (Main/Splash Page)](img/main.PNG "LIS4368 Portal (Main/Splash Page)")

*Failed Validation*:

![Failed Validation](img/p1_fail.PNG "Failed Validation")

*Passed Validation*:

![Passed Validation](img/p1_success.PNG "Passed Validation")