> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Assignment 4 Requirements:

*Sub-Heading:*

1. Implement basic server-side validation
2. Provide screenshot of failed data validation
3. Provide screenshot of successful data validation
4. Provide screenshots of assignment skill sets

#### Assignment Screenshot and Links:

*Screenshot of A4 Failed Validation*:

![A4 Failed Validation](img/a4_fail.PNG "Failed Validation")

*Screenshot of A4 Passed Validation*:

![A4 Passed Validation](img/a4_pass.PNG "Passed Validation")

*Screenshots of Assignment Skill Sets*:

![SS10](img/SS10.PNG "Skill Set 10")

![SS11](img/SS11.PNG "Skill Set 11")

![SS12](img/SS12.PNG "Skill Set 12")