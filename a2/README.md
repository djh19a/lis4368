> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Devin Hartley

### Assignment 2 Requirements:

*Sub-Heading:*

1. Install MySQL Workbench
2. Create ebookshop Database
3. Create Java Servlets
4. Compile Servlets

#### Assignment Screenshots:

*Screenshot of Directory Listing*:

![Directory Listing Screenshot](img/hello.PNG)

*Screenshot of HelloHome.html*:

![HelloHome.html Screenshot](img/HelloHome.PNG)

*Screenshot of Sayhello Servlet*:

![Sayhello Servlet Screenshot](img/sayhello.PNG)

*Screenshot of querybook.html*:

![querybook.html Screenshot](img/querybook.PNG)

*Screenshot of Query Results*:

![Query Results Screenshot](img/queryResults.PNG)

*Screenshot of A2 Website*:

![A2 Website Screenshot](img/a2.PNG)